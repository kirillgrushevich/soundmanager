﻿using System.Collections.Generic;
using UnityEditor;

namespace SoundsManager
{
    [CustomEditor(typeof(AudioClipData))]
    public class AudioClipDataEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var audioClipData = (AudioClipData)target;

            var hashSet = new HashSet<string>();
            
            foreach (var clipInfo in audioClipData.ClipsInfo)
            {
                if (!hashSet.Add(clipInfo.id))
                {
                    clipInfo.id = "Invalid clip Id";
                }
            }
        }
    }
}
