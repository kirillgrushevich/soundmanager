﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SoundsManager
{
    public static class SoundManager
    {
        private static readonly List<AudioClipInfo> AudioClipInfos = new List<AudioClipInfo>();

#if UNITY_EDITOR
        private static AudioClipData[] allClipsData;
#endif

        private static GameObject mainAudioSourceObject;
        private static readonly List<AudioSource> AudioSources = new List<AudioSource>();

        public static void PlayAudioClip(string id)
        {
            var clipInfo = AudioClipInfos.FirstOrDefault(c => c.id == id);

            if (clipInfo == null)
            {
                Debug.LogError($"Clip info with id {id} not found");
                return;
            }
            
            PlayClip(clipInfo);
        }

#if UNITY_EDITOR
        public static string[] GetDataNames()
        {
            allClipsData = Resources.LoadAll<AudioClipData>("Audio");

            var dataNames = allClipsData?.Select(c => c.name);
            return dataNames?.ToArray();
        }
        
        public static string[] GetClipNames(string dataId)
        {
            allClipsData = Resources.LoadAll<AudioClipData>("Audio");

            var clipData = allClipsData.FirstOrDefault(c => c.name == dataId);

            return clipData != null ? clipData.ClipsInfo?.Select(c => c.id).ToArray() : null;
        }
#endif
        
                
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void OnBeforeSceneLoadRuntimeMethod()
        {
            mainAudioSourceObject = new GameObject("Main Audio Source");
            Object.DontDestroyOnLoad(mainAudioSourceObject);

            var newSource = mainAudioSourceObject.AddComponent<AudioSource>();
            AudioSources.Add(newSource);

            var clipsData = Resources.LoadAll<AudioClipData>("Audio");
            foreach (var clipData in clipsData)
            {
                AudioClipInfos.AddRange(clipData.ClipsInfo.ToList());
            }
        }

        private static void PlayClip(AudioClipInfo clipInfo)
        {
            if (clipInfo.clipVariants == null || clipInfo.clipVariants.Length == 0)
            {
                return;
            }

            var clips = clipInfo.clipVariants;
            var clip = clips.Length == 1 ? clips[0] : clips[Random.Range(0, clips.Length)];

            var audioSource = AudioSources.FirstOrDefault(a => !a.isPlaying);
            if (audioSource == null)
            {
                audioSource = mainAudioSourceObject.AddComponent<AudioSource>();
            }

            audioSource.clip = clip;
            audioSource.Play();
        }

    }
}
