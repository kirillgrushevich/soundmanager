﻿using UnityEngine;
using UnityEngine.UI;

namespace SoundsManager
{
    [RequireComponent(typeof(Button))]
    public class ButtonSound : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private string soundId;

#if UNITY_EDITOR
        [SerializeField] private string dataId;
        
        public string DataId
        {
            get => dataId;
            set => dataId = value;
        }

        public string SoundId
        {
            get => soundId;
            set => soundId = value;
        }
#endif
        private void Reset()
        {
            button = GetComponent<Button>();
        }

        private void Start()
        {
            button.onClick.AddListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
            SoundManager.PlayAudioClip(soundId);
        }
    }
}
